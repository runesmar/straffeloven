# Straffeloven 1902 - se [lovdata](https://lovdata.no/dokument/NLO/lov/1902-05-22-10)
# Straffeloven 2005 - se [lovdata](https://lovdata.no/dokument/NL/lov/2005-05-20-28)
* Loven kan anvendes f.eks ved:
    * Innbrudd i datasystemer (§ 145)
        * Dersom det er innført relevante sikringstiltak
        * 1972 - § 405a kan også brukes, men her med lavere straff
    * Egne ansatte røper bedriftshemmeligheter (§ 294)
        * 1972 - Innført tillegg om konsulenter
    * Logiske bomber eller datavirus som medfører skade (§ 291 og §292)
    * Forfalskning, også ved hjelp av datamaskiner (§ 182, §270-271)
    * Innbrudd i datasystemer (§ 204):
      * Med bot eller fengsel inntil 2 år straffes den som ved å bryte en beskyttelse eller ved annen uberettiget fremgangsmåte skaffer seg tilgang til datasystem eller del av det.

    * § 206. Fare for driftshindring:
      * Med bot eller fengsel inntil 2 år straffes den som ved å overføre, skade, slette, forringe, endre, tilføye eller fjerne informasjon uberettiget volder fare for avbrudd eller vesentlig hindring av driften av et datasystem.

    * § 207. Krenkelse av forretningshemmelighet:
      * Med bot eller fengsel inntil 2 år straffes den som har oppnådd kunnskap om eller rådighet over en forretningshemmelighet i anledning av et oppdrag, tillitsverv, eierforhold, tjenesteforhold eller forretningsforhold, og som uberettiget
      a)	gjør bruk av den, for eksempel ved å utnytte den i næringsvirksomhet som konkurrerer med bedriftens egen bruk av hemmeligheten, eller
      b)	gjør den kjent for en annen, med forsett om å sette noen i stand til å dra nytte av den.

  Alt dette står i Kapittel 21. Vern av informasjon og informasjonsutveksling.
